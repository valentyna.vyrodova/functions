const tickets = (queueRaw) => {
  const queue = queueRaw.map((item) => item instanceof String ? parseInt(item) : item)

  const cache = {
      '25': 0,
      '50': 0,
      '100': 0
  }

  for (let i = 0; i < queue.length; i++) {
      const money = queue[i].toString()

      if (money === '25') {
          cache['25'] += 1
      } else if (money === '50') {
          if (cache['25'] > 0) {
              cache['25'] -= 1
              cache['50'] += 1
          } else {
              return 'NO'
          }
      } else {
          if (cache['50'] > 0) {
              if (cache['25'] > 0) {
                  cache['25'] -= 1
                  cache['50'] -= 1
                  cache['100'] += 1
              } else {
                  return 'NO'
              }
          } else if (cache['25'] > 2) {
              cache['25'] -= 3
          } else {
              return 'NO'
          }
      }
  }

  return 'YES'
}

const addStrings = function (num1, num2) {
  let i = num1.length - 1;
  let j = num2.length - 1;
  let carry = 0;
  let sum = '';

  for (; i >= 0 || j >= 0 || carry > 0; i--, j--) {
      const digit1 = i < 0 ? 0 : num1.charAt(i) - '0';
      const digit2 = j < 0 ? 0 : num2.charAt(j) - '0';
      const digitsSum = digit1 + digit2 + carry;
      sum = `${digitsSum % 10}${sum}`;
      carry = Math.floor(digitsSum / 10);
  }
  return sum;
};

const getSum = (a, b) => {
  if (!(typeof a === 'string' && typeof b === 'string')) {
      return false
  } else if (a === '') {
      return b
  } else if (b === '') {
      return a
  }

  if (!/^\d+$/.test(a)) {
      return false
  }
  if (!/^\d+$/.test(b)) {
      return false
  }

  return addStrings(a, b)
}

const getQuantityPostsByAuthor = (listOfPosts, author) => {
  const authorPosts = listOfPosts.filter((p) => p.author === author)
  const comments = listOfPosts.reduce((acc, curr) => {
      const commentNumber = curr?.comments?.filter((c) => c.author === author).length ?? 0
      return commentNumber + acc
  }, 0)
  return `Post:${authorPosts.length},comments:${comments}`
}

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
